test:
	go test -v gitlab.com/cherrug/go-envdir/internal/envdir

fmt:
	gofmt -w -s -d .

vet:
	go vet .

lint:
	golint .

tidy:
	go mod tidy

verify:
	go mod verify

imports:
	goimports -w .

build: fmt vet lint tidy verify imports
	go build -o build/go-envdir