package envdir

import (
	"bufio"
	"bytes"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
	"strings"
)

// collector struct contains
// simple files where:
// 		- their names become a env variable name
// 		- and they contains one line with env value
// dotenvFiles are simple multiline key-value pair text files
type collector struct {
	files       []file
	dotEnvFiles []file
}

type file struct {
	path string
	info os.FileInfo
}

// Collect is an entry point of package
// returns slice of string parsed env variables in format "KEY=VALUE"
func Collect(dir string) ([]string, error) {

	if _, err := os.Stat(dir); os.IsNotExist(err) {
		return nil, fmt.Errorf("directory %s does not exist", dir)
	}

	c := newCollector(dir)

	envs := parse(c)
	return envs, nil
}

func newCollector(dir string) collector {
	c, err := walkDir(dir)
	if err != nil {
		log.Fatalf("failed to walk through dir %s, error %v", dir, err)
	}
	return c
}

func walkDir(dir string) (collector, error) {
	var c collector
	err := filepath.Walk(dir, func(path string, info os.FileInfo, e error) error {
		if !info.IsDir() && info.Mode().IsRegular() {
			if strings.Contains(path, ".env") {
				c.dotEnvFiles = append(c.dotEnvFiles, file{path, info})
			} else {
				c.files = append(c.files, file{path, info})
			}
		}
		return nil
	})

	return c, err
}

// parse skips errors, so if all data failed to parse it returns nil
func parse(c collector) []string {
	var envs []string

	envM := make(map[string]string)

	for _, file := range c.files {
		val := parseOne(file.path)
		if len(val) > 0 {
			envM[file.info.Name()] = val
		}
	}

	for _, file := range c.dotEnvFiles {
		m, err := parseDotFile(file.path)
		if err != nil {
			continue
		}
		for k, v := range m {
			envM[k] = v
		}
	}

	for k, v := range envM {
		envs = append(envs, getKeyValue(k, v))
	}

	return envs
}

func parseOne(file string) string {
	var result string
	dat, err := ioutil.ReadFile(file)
	if err != nil {
		return result
	}
	idx := bytes.IndexByte(dat, '\n')
	if idx < 0 { // file have only one string
		result = string(dat)
	} else {
		result = string(dat[:idx])
	}

	return result
}

func parseDotFile(file string) (map[string]string, error) {
	m := make(map[string]string)

	fh, err := os.Open(file)
	if err != nil {
		return nil, err
	}
	defer fh.Close()

	scanner := bufio.NewScanner(fh)
	for scanner.Scan() {
		data := scanner.Bytes()
		idx := bytes.IndexByte(data, '=')

		if idx < 0 { // skip line if there is no sign =
			continue
		}
		key := string(data[:idx])
		val := string(data[idx+1:])

		if len(key) == 0 || len(val) == 0 {
			continue
		}

		m[key] = val
	}

	return m, nil
}

func getKeyValue(k, v string) string {
	var b strings.Builder
	b.WriteString(k)
	b.WriteString("=")
	b.WriteString(v)
	return b.String()
}
