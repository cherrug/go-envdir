package envdir

import (
	"path"
	"testing"
)

var dir = "testdata"
var aFile = "A_ENV"
var portFile = "PORT"

func TestNewCollector(t *testing.T) {
	c := newCollector(dir)

	if len(c.files) != 3 {
		t.Errorf("c.files want %d, got %d", 3, len(c.files))
	}

	if len(c.dotEnvFiles) != 1 {
		t.Errorf("c.files want %d, got %d", 1, len(c.dotEnvFiles))
	}
}

func TestParseOne(t *testing.T) {
	file := path.Join(dir, "A_ENV")
	s := parseOne(file)

	if len(s) != 7 {
		t.Errorf("file %s expect len %d, got %d", file, 7, len(s))
	}
}

func TestParseOneWithJunk(t *testing.T) {
	file := path.Join(dir, "PORT")
	s := parseOne(file)

	if len(s) != 4 {
		t.Errorf("file %s expect len %d, got %d", file, 4, len(s))
	}
}

func TestParseWithoutNewLine(t *testing.T) {
	file := path.Join(dir, "tmp", "B_ENV")
	s := parseOne(file)

	if len(s) != 1 {
		t.Errorf("file %s expect len %d, got %d", file, 1, len(s))
	}
}

func TestParseDotFIle(t *testing.T) {
	file := path.Join(dir, "dotFiles", "dev.env")
	m, e := parseDotFile(file)
	if e != nil {
		t.Error(e)
	}

	if len(m) != 2 {
		t.Errorf("want %d, got %d", 2, len(m))
	}
}

func TestParse(t *testing.T) {
	c := newCollector(dir)

	envs := parse(c)

	if len(envs) != 4 {
		t.Errorf("want %d but got %d", 4, len(envs))
	}

}
