package cmd

import (
	"log"
	"os"
	"os/exec"

	"gitlab.com/cherrug/go-envdir/internal/envdir"
)

func Exec(dir string, args ...string) {
	envSlice, err := envdir.Collect(dir)
	if err != nil {
		log.Fatalf("failed to parse env files %v", err)
	}

	envSlice = append(envSlice, os.Environ()...)

	lp, err := exec.LookPath(args[0])
	if err != nil {
		log.Fatal(err)
	}

	cmd := exec.Cmd{
		Path:   lp,
		Args:   args,
		Env:    envSlice,
		Stdout: os.Stdout,
		Stderr: os.Stderr,
	}

	err = cmd.Run()
	if err != nil {
		log.Fatal(err)
	}
}
