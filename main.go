package main

import (
	"log"
	"os"

	cmd2 "gitlab.com/cherrug/go-envdir/internal/cmd"
)

func main() {
	if len(os.Args) < 2 {
		log.Fatalln("You must specify at least env_dir and command")
	}
	path := os.Args[1]
	cmd := os.Args[2:]

	cmd2.Exec(path, cmd...)
}
